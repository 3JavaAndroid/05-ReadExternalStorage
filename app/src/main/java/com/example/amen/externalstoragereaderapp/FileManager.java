package com.example.amen.externalstoragereaderapp;

import android.os.Environment;
import android.util.Log;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by amen on 1/18/17.
 */

public enum FileManager {
    INSTANCE;
    private static final String folder_name = "accelerometer";

    /**
     * Czytanie z pliku używając try with resources.
     *
     * @param fileName - pełna ścieżka do pliku
     * @return treść pliku
     */
    public String readFile(String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {

            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }

        } catch (IOException ioe) {
            Log.v(getClass().getName(), ioe.getMessage());
            ioe.printStackTrace();
        }
        return stringBuilder.toString();
    }

    /**
     * Czytanie z pliku używając try with resources.
     *
     * @param fileName - pełna ścieżka do pliku
     * @return treść pliku
     */
    public String readFileScanner(String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        try (Scanner scanner = new Scanner(new FileReader(fileName))) {

            while (scanner.hasNextLine()) {
                stringBuilder.append(scanner.nextLine());
                stringBuilder.append("\n");
            }

        } catch (IOException ioe) {
            Log.v(getClass().getName(), ioe.getMessage());
            ioe.printStackTrace();
        }
        return stringBuilder.toString();
    }

    // listowanie plików z folderu.
    // jeśli folder nie istnieje lub istnieje plik(a nie katalog) o tej nazwie, to tworzymy katalog
    public File[] getFilesFromFolder() {
        String folderPath = Environment.getExternalStorageDirectory() + "/" + folder_name;
        File folder = new File(folderPath);

        if (!folder.exists() || !folder.isDirectory()) {
            folder.mkdir();
        }

        return folder.listFiles();
    }
}
